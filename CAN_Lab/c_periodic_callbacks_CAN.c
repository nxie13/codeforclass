
#include "c_periodic_callbacks_CAN.h"
#include "can.h"
#include "bus_errors.h"
#include "bus_action.h"

can_t CANbus; //initialize the CAN bus

bool c_period_init(void)
{
  CAN_init(CANbus, 100, 64, 64, bus_off_call_back, ((void*)0));
  CAN_bypass_filter_accept_all_msgs(); //accept all msgs
  CAN_reset_bus(CANbus); //start CAN bus
  return true; // Must return true upon success
}

bool c_period_reg_tlm(void)
{
  return true;
}

void c_period_1Hz(uint32_t count)
{
  (void) count;
  if(CAN_is_bus_off(CANbus))
  {
      CAN_reset_bus(CANbus); //start CAN bus
  }
}

void c_period_10Hz(uint32_t count)
{
  (void) count;
   bus_action(CANbus);
}

void c_period_100Hz(uint32_t count)
{
  (void) count;
}

void c_period_1000Hz(uint32_t count)
{
  (void) count;
}
