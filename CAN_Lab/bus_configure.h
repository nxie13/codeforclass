//configuring can bus
#ifndef BUS_CONFIGURE_H_
#define BUS_CONFIGURE_H_

#include "can.h"
#include <stdbool.h>
#include <stdint.h>

bool CAN_bus_configure_11_bits(can_msg_t* CANmsg, uint32_t data_len, can_data_t data,
uint32_t msg_id);

#endif
