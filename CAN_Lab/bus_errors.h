//bus off state -- what to do ?
#ifndef BUS_ERRORS_H_
#define BUS_ERRORS_H_

#include "can.h"
#include <stdint.h>
#include <stdbool.h>

extern can_t CANbus;

void bus_off_call_back(uint32_t);

#endif
