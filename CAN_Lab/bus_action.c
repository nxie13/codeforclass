//bus_action.c
//This board is a receive board, so only the receive section is tested

#include "bus_action.h"

void bus_action(can_t CANbus)
{
  #if 1 //receive
  can_msg_t CANmsg;
  if(CAN_rx(CANbus, &CANmsg, 0))
  {
    if(CANmsg.data.qword == 0xAA)
    {
      //light up LED here
      led_on(1);
    }
    else if (CANmsg.data.qword == 0x00)
    {
      led_off(1);
    }
  }
  #endif

  #if 0 //transmit: msg id 0x1;
  can_msg_t CANmsg;
  can_data_t msg_data;
  msg_data.qword = switch_read(1) ? 0xAA : 0x00;
  if(CAN_bus_configure_11_bits(&CANmsg, 1, msg_data, 0x1))
  {
    CAN_tx(CANmsg, &msg, 0);
  }
  #endif
}
