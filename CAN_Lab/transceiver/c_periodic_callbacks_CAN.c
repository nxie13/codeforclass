#ifdef UNIT_TESTING
#define static /* blank */
#else
#define static static
#endif

#include "c_periodic_callbacks_CAN.h"
#include "can.h"
#include "can_action.h"


bool c_period_init(void)
{
    if (CAN_init(can1, 100, 64, 64, (void*)0, (void*)0))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can1);
        return true;
    }
    else {
        return false;
    }
}


bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    //nothing here
    (void) count;
    can_check_and_reset(can1);
}

void c_period_10Hz(uint32_t count) //100ms
{
    (void) count;
    can_transmit(can1);

}

void c_period_100Hz(uint32_t count)
{
    //empty
    (void) count;
}

void c_period_1000Hz(uint32_t count)
{
    //empty
    (void) count;
}
