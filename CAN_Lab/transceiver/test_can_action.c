#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "can_action.h"
#include "Mockswitch_wrapper.h"
#include "MockLED_display_wrapper.h"
#include "Mockcan.h"
#include "Mockcan_msg_validation.h"
#include "unit_test_facilitator.h"

can_t can_bus_test;
can_msg_t can_msg_test;


void setUp(void) {

}
void tearDown(void) {
}

void test_can_transmit(void){
    //Test Case: When 0xAA is sent successfully (Switch pressing)
    switch_read_ExpectAndReturn(1,true);
    can_msg_validate_ExpectAndReturn(can_msg_test,true);
    can_msg_validate_IgnoreArg_can_msg();
    CAN_tx_ExpectAndReturn(can_bus_test, &can_msg_test, 0,true);
    CAN_tx_IgnoreArg_can();
    CAN_tx_IgnoreArg_msg();
    //CAN_tx_IgnoreArg_pCanMsg();
    TEST_ASSERT_TRUE(can_transmit(can_bus_test));

    //Test Case: When 0x00 is sent (Switch not pressed)
    switch_read_ExpectAndReturn(1,false);
    can_msg_validate_ExpectAndReturn(can_msg_test,false);
    can_msg_validate_IgnoreArg_can_msg();
    TEST_ASSERT_FALSE(can_transmit(can_bus_test));
}

void test_can_check_and_reset(void){
    //Test Case: If the can bus is off.
    CAN_is_bus_off_ExpectAndReturn(can_bus_test,true);
    CAN_reset_bus_Expect(can_bus_test);
    TEST_ASSERT_TRUE(can_check_and_reset(can_bus_test));

    //Test Case: If the can bus is on.
    CAN_is_bus_off_ExpectAndReturn(can_bus_test,false);
    TEST_ASSERT_FALSE(can_check_and_reset(can_bus_test));
}