#include <stdint.h>
#include <stdbool.h>
#include "can_msg_validation.h"

bool can_msg_validate(can_msg_t can_msg){
    // Return false if the data_length is greater than 8 bytes
    if (can_msg.frame_fields.data_len > 8)
        return false;
    //Check if the data size is actually smaller than or equal to the data size declared in data_len. Return false if not.
    uint32_t mask1 = 0xFFFFFFFF; //Upper 32 bits
    uint32_t mask2 = 0xFFFFFFFF; //Lower 32 bits

    if (can_msg.frame_fields.data_len <= 4) {
        for (int i = 0; i < ((can_msg.frame_fields.data_len) * 8); i++) {
            mask2 = mask2 << 1;
        }
    }
    else {
        mask2 = 0x0;
        for (int i = 0; i < (((can_msg.frame_fields.data_len)-4) * 8); i++) {
            mask1 = mask1 << 1;
        }
    }
    if (((mask1 & can_msg.data.dwords[1]) != 0) || ((mask2 & can_msg.data.dwords[0]) != 0))
        return false;
    //Check if the message id is shorter than or equal to 11 bits. Return false if greater than 11 bits.
    uint32_t can_msg_mask_2 = 0xFFFFF800;
    if ((can_msg.msg_id & can_msg_mask_2) != 0)
        return false;

    return true;
}

