/**
 * @{
 * @name Macros that give different meaning depending on if we are compiling for UTs or production code
 *
 * For Unit-Tests, we do not define any meaning to 'static' because it is intentional
 * to not hide private variables and functions for the sake of unit-testing.
 *
 * This may create a problem with local variables of a function that are static, but
 * our coding standards forbid such use.
 *
 * In any case, this only affects our unit-tests, and not production code
 */
#ifdef UNIT_TESTING
  #define const         /* blank */
  #define static        /* blank */
#else
  #define const         const
  #define static        static
#endif
/** @} */
