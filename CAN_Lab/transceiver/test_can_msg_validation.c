#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "Mockcan_action.h"
#include "Mockswitch_wrapper.h"
#include "MockLED_display_wrapper.h"
#include "Mockcan.h"
#include "can_msg_validation.h"
#include "unit_test_facilitator.h"

can_msg_t can_msg_test;

void setUp(void) {
}
void tearDown(void) {
}

void test_can_msg_validate(void) {
    //Test Case: can_msg.frame_fields.data_len > 8 and Return False;
    can_msg_test.frame_fields.data_len = 9;
    TEST_ASSERT_FALSE(can_msg_validate(can_msg_test));

    //Test Case: can_msg.frame_fields.data_len < 8 but the data that user tries to send is greater than data length declared
    can_msg_test.frame_fields.data_len = 6;
    can_msg_test.data.qword = 0x0FEDCBA987654321;
    TEST_ASSERT_FALSE(can_msg_validate(can_msg_test));

    can_msg_test.frame_fields.data_len = 2;
    can_msg_test.data.qword = 0x0000000000054321;
    TEST_ASSERT_FALSE(can_msg_validate(can_msg_test));

    //Test Case: Message id is longer than 11-bits (Assuming we do not use 29-bits)
    //Return false when can_msg id is greater than 11 bits.
    can_msg_test.frame_fields.data_len = 3;
    can_msg_test.data.qword = 0x00000000000000AA;
    can_msg_test.msg_id = 0x821;
    TEST_ASSERT_FALSE(can_msg_validate(can_msg_test));

    //Return true if it passes all the previous test:
    can_msg_test.frame_fields.data_len = 3;
    can_msg_test.data.qword = 0x00000000000000AA;
    can_msg_test.msg_id = 0x321;
    TEST_ASSERT_TRUE(can_msg_validate(can_msg_test));
}