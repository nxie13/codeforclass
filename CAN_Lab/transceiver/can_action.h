#ifndef CAN_ACTION_H__
#define CAN_ACTION_H__

#include <stdbool.h>
#include <stdint.h>
#include "can.h"

#ifdef __cplusplus
extern "C" {
#endif

bool can_transmit(can_t can_bus);
bool can_check_and_reset(can_t can_bus);

#ifdef __cplusplus
}
#endif
#endif /* SWITCH_WRAPPER_H__ */
