//test_bus_action - testing the action of the bus
#include "unity.h"
#include "bus_action.h"
#include "Mockcan.h"
#include "bus_configure.h"
#include "Mockled_display_wrapper.h"

can_t CANbus;
can_msg_t can_message;

  void test_bus_action(void)
  {
    //1. when the message is 0XAA
    can_message.data.qword = 0xAA;
    CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
    CAN_rx_IgnoreArg_can();
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&can_message);
    led_on_Expect(1);
    bus_action(CANbus);

    //2. when the message is 0X00;
    can_message.data.qword = 0x00;
    CAN_rx_ExpectAndReturn(CANbus, &can_message, 0, true);
    CAN_rx_IgnoreArg_can();
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&can_message);
    led_off_Expect(1);
    bus_action(CANbus);

    //3. when the message is is neither
    can_message.data.qword = 0xBB;
    CAN_rx_ExpectAndReturn(CANbus, &can_message, 0, true);
    CAN_rx_IgnoreArg_can();
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&can_message);
    bus_action(CANbus);

    //4. when the message didn't get received
    can_message.data.qword = 0xBB;
    CAN_rx_ExpectAndReturn(CANbus, &can_message, 0, false);
    CAN_rx_IgnoreArg_can();
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&can_message);
    bus_action(CANbus);
  }
