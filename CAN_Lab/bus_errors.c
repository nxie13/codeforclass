//bus_off.c

#include "bus_errors.h"

//This is the bus off callback function, when the bus gets off,
//the function will be called
void bus_off_call_back(uint32_t ICR_reg)
{
  ICR_reg++; //for testing
  //turn it back on at 1 Hz
  CAN_reset_bus(CANbus); //start CAN bus
}
