//configure can messages

#include "bus_configure.h"

bool CAN_bus_configure_11_bits(can_msg_t* CANmsg, uint32_t data_len, can_data_t data,
  uint32_t msg_id)
  {
    if(data_len > 8)
    {
      return false; //data length cannot be larger than 8 bytes
    }
    //check if data is within data length
    uint64_t data_mask = 0x0;
    for(uint64_t i = 0; i < data_len*8; i++)
    {
      data_mask |= (0x1 << i);
    }
    if((data_mask & data.qword) < data.qword)
    return false; //if data is more than the required length

    //check if message id is within 11 bits
    data_mask = 0x7f; //11 1's
    if((data_mask & msg_id) < msg_id)
    return false; //if message id is more than the required length

    CANmsg->msg_id = msg_id;
    CANmsg->frame_fields.data_len = data_len;
    CANmsg->data = data;
    return true;
  }
