//test bus test_bus_configuration
#include "unity.h"
#include "can.h"
#include "bus_configure.h"

void test_message_configuration(void)
{
  can_msg_t test_msg;
  can_data_t data;
  data.qword = 0xFFFF;
  //test the case when data is more than required length
  TEST_ASSERT_FALSE(CAN_bus_configure_11_bits(&test_msg, 9, data, 0x1));
  //test the case when message id is more than required length
  TEST_ASSERT_FALSE(CAN_bus_configure_11_bits(&test_msg, 1, data, 0xFFFF)); //here message id is 16 bits
  //test to see when all the data given are correct, is the message correct
  data.qword = 0xAA;
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&test_msg, 1, data, 0x1));
  TEST_ASSERT_EQUAL(0xAA, test_msg.data.qword);
  TEST_ASSERT_EQUAL(0x1, test_msg.msg_id);
}
