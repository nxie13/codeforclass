//bus_action.h

#ifndef BUS_ACTION_H_
#define BUS_ACTION_H_
#include "can.h"
#include "bus_configure.h"
#include "led_display_wrapper.h"
//#include "switch_wrapper.h" only for transmitter

void bus_action(can_t CANbus);

#endif
