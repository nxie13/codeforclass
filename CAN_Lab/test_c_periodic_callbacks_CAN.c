//test for CAN lab
#include "unity.h"
#include "Mockbus_errors.h"
#include "Mockbus_configure.h"
#include "Mockbus_action.h"
#include "Mockcan.h"
#include "Mockled_display_wrapper.h"
#include "c_periodic_callbacks_CAN.h"

void test_c_period_init(void)
{
  CAN_init_ExpectAndReturn(CANbus, 100, 64, 64, NULL, NULL, true);
  CAN_init_IgnoreArg_can();
  CAN_init_IgnoreArg_bus_off_cb();
  CAN_init_IgnoreArg_data_ovr_cb();

  CAN_bypass_filter_accept_all_msgs_Expect(); //accept all msgs
  CAN_reset_bus_ExpectAnyArgs();

  c_period_init();
}

void test_c_period_reg_tlm(void)
{
  TEST_ASSERT_TRUE(c_period_reg_tlm());
}

void test_c_period_1Hz(void)
{
  CAN_is_bus_off_ExpectAnyArgsAndReturn(true);
  CAN_reset_bus_ExpectAnyArgs();
  c_period_1Hz(0);

  CAN_is_bus_off_ExpectAnyArgsAndReturn(false);
  c_period_1Hz(0);
}

void test_c_period_10Hz(void)
{
  bus_action_ExpectAnyArgs();
  c_period_10Hz(0);
}
