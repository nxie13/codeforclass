#include "c_periodic_callbacks_CAN_dbc.h"
#include "switch_wrapper.h"

//can_msg_t can_msg = { 0 };
bool c_period_init(void)
{
    //
    
    if (CAN_init(can1, 100, 64, 64, (void*) 0, (void*) 0)) {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(0);
        return true;
    }
    else {
        return false;
    }
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    //nothing here
    (void) count;
    if(CAN_is_bus_off(0)){
        CAN_reset_bus(0);
    }
}

void c_period_10Hz(uint32_t count) //100ms
{

    (void) count;
    can_msg_t can_msg = { 0 };
    SENSOR_BOARD_t sensor_cmd = { 0 };
    sensor_cmd.SENSOR_VALUE = 0x11;

    if (switch_read(1)) {
        sensor_cmd.SENSOR_VALUE = 0xAA;
    }
    else if (switch_read(2)) {
        sensor_cmd.SENSOR_VALUE = 0xBB;
    }
    else if (switch_read(3)) {
        sensor_cmd.SENSOR_VALUE = 0xCC;
    }
    else if (switch_read(4)) {
        sensor_cmd.SENSOR_VALUE = 0xDD;
    }

    dbc_msg_hdr_t msg_hdr;
    msg_hdr = dbc_encode_SENSOR_BOARD(can_msg.data.bytes, &sensor_cmd);
    can_msg.msg_id = msg_hdr.mid;
    can_msg.frame_fields.data_len = msg_hdr.dlc;

    CAN_tx(can1, &can_msg, 0);

}

void c_period_100Hz(uint32_t count)
{
    //empty
    (void) count;
}

void c_period_1000Hz(uint32_t count)
{
    //empty
    (void) count;
}
