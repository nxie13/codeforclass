/// DBC file: ../_can_dbc/243.dbc    Self node: 'DRIVER'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __GENEARTED_DBC_PARSER
#define __GENERATED_DBC_PARSER
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/// Extern function needed for dbc_encode_and_send()
extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

/// Missing in Action structure
typedef struct {
    uint32_t is_mia : 1;          ///< Missing in action flag
    uint32_t mia_counter_ms : 31; ///< Missing in action counter
} dbc_mia_info_t;

/// CAN message header structure
typedef struct { 
    uint32_t mid; ///< Message ID of the message
    uint8_t  dlc; ///< Data length of the message
} dbc_msg_hdr_t; 

static const dbc_msg_hdr_t SENSOR_BOARD_HDR =                     {  200, 4 };




/// Message: SENSOR_BOARD from 'DRIVER', DLC: 4 byte(s), MID: 200
typedef struct {
    uint32_t SENSOR_VALUE;                    ///< B31:0   Destination: SENSOR

    // No dbc_mia_info_t for a message that we will send
} SENSOR_BOARD_t;


/// @{ These 'externs' need to be defined in a source file of your project
/// @}


/// Encode DRIVER's 'SENSOR_BOARD' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_SENSOR_BOARD(uint8_t bytes[8], SENSOR_BOARD_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->SENSOR_VALUE)))) & 0xffffffff;
    bytes[0] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B0
    bytes[1] |= (((uint8_t)(raw >> 8) & 0xff)); ///< 8 bit(s) starting from B8
    bytes[2] |= (((uint8_t)(raw >> 16) & 0xff)); ///< 8 bit(s) starting from B16
    bytes[3] |= (((uint8_t)(raw >> 24) & 0xff)); ///< 8 bit(s) starting from B24

    return SENSOR_BOARD_HDR;
}

/// Encode and send for dbc_encode_SENSOR_BOARD() message
static inline bool dbc_encode_and_send_SENSOR_BOARD(SENSOR_BOARD_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_SENSOR_BOARD(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_decode_SENSOR_BOARD() since 'DRIVER' is not the recipient of any of the signals

#endif
