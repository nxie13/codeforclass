#include "unity.h" // Single Unity Test Framework include
#include "Mockswitch_wrapper.h"
#include "MockLED_display_wrapper.h"

#include "unit_test_facilitator.h"
#include "c_periodic_callbacks_CAN_dbc.h"
#include "Mockcan.h"
//#include "generated_can.h"

can_t can_bus_test;
can_msg_t can_msg_test;
SENSOR_BOARD_t sensor_cmd_test;
dbc_msg_hdr_t msg_hdr_test;

void setUp(void)
{
}
void tearDown(void)
{
}

void test_c_period_init(void)
{

    //Test Case: CAN_init is true
    //CAN_init_ExpectAndReturn(can1, 100, 64, 64, NULL, NULL, true);
    CAN_init_ExpectAndReturn(can1, 100, 64, 64, (void *)0, (void *)0, true);
    CAN_init_IgnoreArg_can();
    CAN_init_IgnoreArg_bus_off_cb();
    CAN_init_IgnoreArg_data_ovr_cb();
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    CAN_reset_bus_IgnoreArg_can();
    c_period_init();

    //TEST_ASSERT_TRUE(c_period_init());
    //Test Case: CAN_init is false
    CAN_init_ExpectAndReturn(can1, 100, 64, 64, NULL, NULL, false);
    c_period_init();
}

void test_c_period_reg_tlm(void)
{

    TEST_ASSERT_TRUE(c_period_reg_tlm());
}

void test_c_period_1Hz(void)
{
    //Test Case: If the can bus is off.
    CAN_is_bus_off_ExpectAndReturn(can_bus_test, true);
    CAN_reset_bus_Expect(can_bus_test);
    c_period_1Hz(0);

    //Test Case: If the can bus is on.
    CAN_is_bus_off_ExpectAndReturn(can_bus_test, false);
    c_period_1Hz(0);
}

void test_c_period_10Hz_sent0xAA(void)
{
    //Test Case: When 0xAA is sent successfully (Switch pressing)
    
    switch_read_ExpectAndReturn(1, true);
    //sensor_cmd_test.SENSOR_VALUE = 0xAA;
    TEST_ASSERT_EQUAL(0xAA,sensor_cmd_test.SENSOR_VALUE);
    can_msg_t can_msg_test = {0};
    SENSOR_BOARD_t sensor_cmd_test = {0};
    msg_hdr_test = dbc_encode_SENSOR_BOARD(can_msg_test.data.bytes, &sensor_cmd_test);

    can_msg_test.msg_id = msg_hdr_test.mid;
    can_msg_test.frame_fields.data_len = msg_hdr_test.dlc;
    CAN_tx_ExpectAndReturn(can_bus_test, &can_msg_test, 0, true);
    CAN_tx_IgnoreArg_can();
    CAN_tx_IgnoreArg_msg();
    c_period_10Hz(0);
    
}

void test_c_period_10Hz_sent0xBB(void)
{
    //Test Case: When 0xAA is sent successfully (Switch pressing)
    
    switch_read_ExpectAndReturn(1, false);
    switch_read_ExpectAndReturn(2, true);
    sensor_cmd_test.SENSOR_VALUE = 0xBB;
    //TEST_ASSERT_EQUAL(0xAA,sensor_cmd_test.SENSOR_VALUE);
    can_msg_t can_msg_test = {0};
    SENSOR_BOARD_t sensor_cmd_test = {0};
    msg_hdr_test = dbc_encode_SENSOR_BOARD(can_msg_test.data.bytes, &sensor_cmd_test);

    can_msg_test.msg_id = msg_hdr_test.mid;
    can_msg_test.frame_fields.data_len = msg_hdr_test.dlc;
    CAN_tx_ExpectAndReturn(can_bus_test, &can_msg_test, 0, true);
    CAN_tx_IgnoreArg_can();
    CAN_tx_IgnoreArg_msg();
    c_period_10Hz(0);
    
}

void test_c_period_10Hz_sent0xCC(void)
{
    //Test Case: When 0xAA is sent successfully (Switch pressing)
    
    switch_read_ExpectAndReturn(1, false);
    switch_read_ExpectAndReturn(2, false);
    switch_read_ExpectAndReturn(3, true);
    sensor_cmd_test.SENSOR_VALUE = 0xAA;
    //TEST_ASSERT_EQUAL(0xAA,sensor_cmd_test.SENSOR_VALUE);
    can_msg_t can_msg_test = {0};
    SENSOR_BOARD_t sensor_cmd_test = {0};
    msg_hdr_test = dbc_encode_SENSOR_BOARD(can_msg_test.data.bytes, &sensor_cmd_test);

    can_msg_test.msg_id = msg_hdr_test.mid;
    can_msg_test.frame_fields.data_len = msg_hdr_test.dlc;
    CAN_tx_ExpectAndReturn(can_bus_test, &can_msg_test, 0, true);
    CAN_tx_IgnoreArg_can();
    CAN_tx_IgnoreArg_msg();
    c_period_10Hz(0);
    
}

void test_c_period_10Hz_sent0xDD(void)
{
    //Test Case: When 0xAA is sent successfully (Switch pressing)
    switch_read_ExpectAndReturn(1, false);
    switch_read_ExpectAndReturn(2, false);
    switch_read_ExpectAndReturn(3, false);
    switch_read_ExpectAndReturn(4, true);
    sensor_cmd_test.SENSOR_VALUE = 0xDD;
    //TEST_ASSERT_EQUAL(0xAA,sensor_cmd_test.SENSOR_VALUE);
    can_msg_t can_msg_test = {0};
    SENSOR_BOARD_t sensor_cmd_test = {0};
    msg_hdr_test = dbc_encode_SENSOR_BOARD(can_msg_test.data.bytes, &sensor_cmd_test);

    can_msg_test.msg_id = msg_hdr_test.mid;
    can_msg_test.frame_fields.data_len = msg_hdr_test.dlc;
    CAN_tx_ExpectAndReturn(can_bus_test, &can_msg_test, 0, true);
    CAN_tx_IgnoreArg_can();
    CAN_tx_IgnoreArg_msg();
    c_period_10Hz(0);
    
}

void test_c_period_100Hz(void)
{
    c_period_100Hz(0);
}

void test_c_period_1000Hz(void)
{
    c_period_1000Hz(0);
}
