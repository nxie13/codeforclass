#ifndef C_PERIODIC_CALLBACKS_H__
#define C_PERIODIC_CALLBACKS_H__


#include <stdbool.h>
#include <stdint.h>
#include "Z:\CMPE243\SJSU_Dev\projects\lpc1758_freertos - CAN_dbc\_can_dbc\generated_can.h"
#include "can.h"
//#include "generated_can.h"

extern can_t can_bus_test;
extern can_msg_t can_msg_test;
//extern SENSOR_BOARD_t sensor_cmd_test;
//extern dbc_msg_hdr_t msg_hdr_test;

#ifdef __cplusplus
extern "C" {
#endif

bool c_period_init(void);

bool c_period_reg_tlm(void);

void c_period_1Hz(uint32_t count); //1s

void c_period_10Hz(uint32_t count); //100ms

void c_period_100Hz(uint32_t count); //10ms

void c_period_1000Hz(uint32_t count); //1ms

#ifdef __cplusplus
}
#endif
#endif /* C_PERIOD_CALLBACKS_H__ */
