#include "c_periodic_callbacks_dbc.h"

// The MIA functions require that you define the:
//   - Time when the handle_mia() functions will replace the data with the MIA
//   - The MIA data itself (ie: MOTOR_STATUS__MIA_MSG)

const uint32_t SENSOR_BOARD__MIA_MS = 3000;
const SENSOR_BOARD_t SENSOR_BOARD__MIA_MSG = { .SENSOR_VALUE = 0xFF };

SENSOR_BOARD_t sensor_msg_dbc = {0};
bool c_period_init(void)
{
    CAN_init(CANbus, 100, 64, 64, ((void*) 0), ((void*) 0));
    CAN_bypass_filter_accept_all_msgs(); //accept all msgs
    CAN_reset_bus(CANbus); //start CAN bus
    return true; // Must return true upon success
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    (void) count;
    if (CAN_is_bus_off(CANbus)) {
        CAN_reset_bus(CANbus); //start CAN bus
    }
}

void c_period_10Hz(uint32_t count)
{
    (void) count;
    can_msg_t CANmsg;

    while (CAN_rx(CANbus, &CANmsg, 0)) {
        // Form the message header from the metadata of the arriving message
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = CANmsg.frame_fields.data_len;
        can_msg_hdr.mid = CANmsg.msg_id;

        //decode the message
        dbc_decode_SENSOR_BOARD(&sensor_msg_dbc, CANmsg.data.bytes, &can_msg_hdr);
    }

    // Service the MIA counter of a regular (non MUX'd) message
    dbc_handle_mia_SENSOR_BOARD(&sensor_msg_dbc, 100);

    if (sensor_msg_dbc.SENSOR_VALUE == 0xAA) { //button 1
        led_display_set_number('1');
        led_off(4);
    }
    else if (sensor_msg_dbc.SENSOR_VALUE == 0xBB) { //button 2
        led_display_set_number('2');
        led_off(4);
    }
    else if (sensor_msg_dbc.SENSOR_VALUE == 0xCC) { //button 3
        led_display_set_number('3');
        led_off(4);
    }
    else if (sensor_msg_dbc.SENSOR_VALUE == 0xDD) { //button 4
        led_display_set_number('4');
        led_off(4);
    }

    else if (sensor_msg_dbc.SENSOR_VALUE == 0xFF) { //button 1
        led_display_clear();
        led_on(4);
    }
    else if (sensor_msg_dbc.SENSOR_VALUE == 0x11) { //when tx board is idle, send 0x11
        led_display_set_number('0');
        led_off(4);
    }
}

void c_period_100Hz(uint32_t count)
{
    (void) count;
}

void c_period_1000Hz(uint32_t count)
{
    (void) count;
}
