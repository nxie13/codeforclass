#include "unity.h"
#include "c_periodic_callbacks_dbc.h"
#include "bus_configure.h" //uses this to configure the CANbus object
#include "Mockcan.h"
#include "Mockled_display_wrapper.h"

can_msg_t CANmsg;

void test_c_period_init(void)
{
  CAN_init_ExpectAndReturn(CANbus, 100, 64, 64, NULL, NULL, true);
  CAN_init_IgnoreArg_can();
  CAN_init_IgnoreArg_bus_off_cb();
  CAN_init_IgnoreArg_data_ovr_cb();

  CAN_bypass_filter_accept_all_msgs_Expect(); //accept all msgs
  CAN_reset_bus_ExpectAnyArgs();

  c_period_init();
}

void test_c_period_reg_tlm(void)
{
  TEST_ASSERT_TRUE(c_period_reg_tlm());
}

void test_c_period_1Hz(void)
{
  CAN_is_bus_off_ExpectAnyArgsAndReturn(true);
  CAN_reset_bus_ExpectAnyArgs();
  c_period_1Hz(0);

  CAN_is_bus_off_ExpectAnyArgsAndReturn(false);
  c_period_1Hz(0);
}

void test_c_period_10Hz_0xAA_msg(void)
{
  can_data_t can_data = {0xAA};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(200, CANmsg.msg_id);
  TEST_ASSERT_EQUAL(4, CANmsg.frame_fields.data_len);
  TEST_ASSERT_EQUAL(0xAA, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);

  //exit while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();

  led_display_set_number_Expect('1');
  led_off_Expect(4);

  c_period_10Hz(0);
}

void test_c_period_10Hz_0xBB_msg(void)
{
  can_data_t can_data = {0xBB};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(0xBB, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);

  //exit while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();

  led_display_set_number_Expect('2');
  led_off_Expect(4);

  c_period_10Hz(0);
}

void test_c_period_10Hz_0xCC_msg(void)
{
  can_data_t can_data = {0xCC};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(0xCC, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);

  //exit while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();

  led_display_set_number_Expect('3');
  led_off_Expect(4);

  c_period_10Hz(0);
}

void test_c_period_10Hz_0xDD_msg(void)
{
  can_data_t can_data = {0xDD};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(0xDD, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);

  //exit while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();

  led_display_set_number_Expect('4');
  led_off_Expect(4);

  c_period_10Hz(0);
}

void test_c_period_10Hz_loop(void)
{
  can_data_t can_data = {0x1};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(0x01, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);


  //run the test until it is right before MIA
  for(int i = 1; i < 31; i++)
  {
    CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
    CAN_rx_IgnoreArg_can();
    CAN_rx_IgnoreArg_msg();

    c_period_10Hz(0);

    TEST_ASSERT_EQUAL(100*(i), sensor_msg_dbc.mia_info.mia_counter_ms);
  }

  //now it should be in MIA state
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  led_display_clear_Expect();
  led_on_Expect(4);
  c_period_10Hz(0);

  TEST_ASSERT_EQUAL(1, sensor_msg_dbc.mia_info.is_mia);
  TEST_ASSERT_EQUAL(0xFF, sensor_msg_dbc.SENSOR_VALUE);
}

void test_c_period_10Hz_0x11_msg(void)
{
  can_data_t can_data = {0x11};

  //Set CANmsg and test the CAN_BUS_configure function
  TEST_ASSERT_TRUE(CAN_bus_configure_11_bits(&CANmsg, 4, can_data, 200));
  TEST_ASSERT_EQUAL(0x11, CANmsg.data.dwords[0]);

  //entering while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, true);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();
  CAN_rx_ReturnThruPtr_msg(&CANmsg);

  //exit while loop
  CAN_rx_ExpectAndReturn(CANbus, NULL, 0, false);
  CAN_rx_IgnoreArg_can();
  CAN_rx_IgnoreArg_msg();

  led_display_set_number_Expect('0');
  led_off_Expect(4);

  c_period_10Hz(0);
}
