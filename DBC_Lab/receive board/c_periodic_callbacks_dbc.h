#ifndef C_PERIODIC_CALLBACKS_DBC_H__
#define C_PERIODIC_CALLBACKS_DBC_H__

#include <stdbool.h>
#include <stdint.h>
#include "C:\CMPE146\SJSU_Dev\projects\lpc1758_freertos -243DBC\_can_dbc\generated_can.h"
#include "can.h"
#include "bus_configure.h"
#include "led_display_wrapper.h"

#ifdef __cplusplus
extern "C" {
#endif

can_t CANbus;

extern SENSOR_BOARD_t sensor_msg_dbc;

bool c_period_init(void);

bool c_period_reg_tlm(void);

void c_period_1Hz(uint32_t count); //1s

void c_period_10Hz(uint32_t count); //100ms

void c_period_100Hz(uint32_t count); //10ms

void c_period_1000Hz(uint32_t count); //1ms

#ifdef __cplusplus
}
#endif
#endif /* C_PERIOD_CALLBACKS_DBC_H__ */
