#include "unity.h"
#include "steer_processor.h"

#include "Mocksteering.h"

void test_steer_processor__move_left(void) {
  extern uint8_t threshold;
  TEST_ASSERT_EQUAL(50, threshold);
  steer_left_Expect(); //I want to turn left
  steer_processor(60, 20);
  steer_left_Expect(); //I want to turn left
  steer_processor(70, 40);
}

void test_steer_processor__move_right(void) {
  steer_right_Expect(); //I want to turn right
  steer_processor(10, 60);
  steer_right_Expect(); //I want to turn right
  steer_processor(40, 80);
}

void test_steer_processor__both_sensors_less_than_threshold(void) {
  steer_right_Expect();
  steer_processor(10, 20);
  steer_right_Expect();
  steer_processor(40, 40);
  steer_left_Expect();
  steer_processor(40, 20);
}

void test_steer_processor__do_not_move(void) {
  steer_processor(70, 80);
  steer_processor(50, 50);
  steer_processor(70, 50);
}

// Do not modify this test case
// Modify your implementation of steer_processor() to make it pass
void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}
