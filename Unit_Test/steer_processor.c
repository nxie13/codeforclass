#include "steer_processor.h"
#include "unit_test_facilitator.h"

static uint32_t threshold = 50;

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm)
{
  if(left_sensor_cm >= threshold && right_sensor_cm < threshold)
  {
      steer_left();
  }
  else if (right_sensor_cm >= threshold && left_sensor_cm < threshold)
  {
      steer_right();
  }
  else if (right_sensor_cm < threshold && left_sensor_cm < threshold)
  {
    if(left_sensor_cm > right_sensor_cm)
    {
      steer_left();
    }
    else
    {
      steer_right();
    }
  }
}
