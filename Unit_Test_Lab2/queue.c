//queue.c file
#include "queue.h"

void queue__init(queue_S *queue)
{
  //when initialized, the memory should only have '\0' in it
  // and count should be 0
  queue->queue_memory[0] = '\0';
  queue->queue_count = 0;
}

bool queue__push(queue_S *queue, uint8_t push_value)
{
  //size of queue is where the '\0' location is
  int size_of_queue = queue->queue_count;
  if(size_of_queue == 99)
  {
    return false; //can't push anymore numbers
  }

  for(int i = size_of_queue; i >= 0; i--) //shifting everything right 1 space
  {
    queue->queue_memory[i+1] = queue->queue_memory[i];
  }
  queue->queue_memory[0] = push_value;
  queue->queue_count++;
  return true;
}

bool queue__pop(queue_S *queue, uint8_t *pop_value)
{
  if(queue->queue_count == 0)
  {
    return false; //nothing to pop
  }
  queue->queue_count --;
  *pop_value = queue->queue_memory[queue->queue_count];
  queue->queue_memory[queue->queue_count] = '\0';
  return true;
}

uint32_t queue__get_count(queue_S *queue)
{
  return queue->queue_count;
}
