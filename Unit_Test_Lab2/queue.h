// @file queue.h
#include <stdint.h>
#include <stdbool.h>

typedef struct {
  uint8_t queue_memory[100];
  uint32_t queue_count;
} queue_S;

void queue__init(queue_S *queue);

bool queue__push(queue_S *queue, uint8_t push_value);

bool queue__pop(queue_S *queue, uint8_t *pop_value);

uint32_t queue__get_count(queue_S *queue);
