//test_queue.c file
#include "unity.h"
#include "queue.h"
#include <stdbool.h>
#include <stdint.h>


queue_S queue_to_test;

void test_queue__init(void)
{
  queue__init(&queue_to_test);
  TEST_ASSERT_EQUAL('\0', queue_to_test.queue_memory[0]);
  TEST_ASSERT_EQUAL(0, queue__get_count(&queue_to_test));
}

void test_queue_push_and_count(void)
{
  for(int i = 0; i < 10; i++) //push 10 chars to the queue
  {
    uint8_t char_to_push = '0' + i;
    queue__push(&queue_to_test, char_to_push); //push character
    TEST_ASSERT_EQUAL(char_to_push, queue_to_test.queue_memory[0]); //check if it's in queue
    TEST_ASSERT_EQUAL(i+1, queue__get_count(&queue_to_test)); //check queue count
  }
  TEST_ASSERT_EQUAL('\0', queue_to_test.queue_memory[10]);
}

void test_queue_pop_and_count(void)
{
  for(int i = 0; i < 10; i++) //pop the 10 chars from the queue
 {
    uint8_t char_popped_out;
    queue__pop(&queue_to_test, &char_popped_out);
    TEST_ASSERT_EQUAL((uint8_t)(i + '0'), char_popped_out); //check if the popped char is right
    TEST_ASSERT_EQUAL(9 - i, queue__get_count(&queue_to_test)); //check queue count
 }
}

void test_queue_push_should_return_false(void)
{
  //test an empty queue
  uint8_t character_temp;
  TEST_ASSERT_FALSE(queue__pop(&queue_to_test, &character_temp));
}

void test_queue_pop_should_return_false(void)
{
  //test a full queue
  for(int i = 0; i < 99; i++)
  {
    queue__push(&queue_to_test, '0'); //push character until full
  }
  TEST_ASSERT_FALSE(queue__push(&queue_to_test, '0'));
}
